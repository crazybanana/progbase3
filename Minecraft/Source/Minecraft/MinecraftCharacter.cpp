// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Minecraft.h"
#include "MinecraftCharacter.h"
#include "MinecraftProjectile.h"
#include "MySaveGame.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AMinecraftCharacter

AMinecraftCharacter::AMinecraftCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_WieldedItem = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_WieldedItem"));
	FP_WieldedItem->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_WieldedItem->bCastDynamicShadow = false;
	FP_WieldedItem->CastShadow = false;
	// FP_WieldedItem->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_WieldedItem->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_WieldedItem);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_WieldedItem, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.


	Reach = 300.f;

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	Inventory.SetNum(NUM_OF_INVENTORY_SLOTS);
}

void AMinecraftCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	//FP_WieldedItem->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

}

void AMinecraftCharacter::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	CheckForBlocks();

}

//////////////////////////////////////////////////////////////////////////
// Input

void AMinecraftCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("InventoryUp", IE_Pressed, this, &AMinecraftCharacter::MoveDownInventorySlot);
	PlayerInputComponent->BindAction("InventoryDown", IE_Released, this, &AMinecraftCharacter::MoveUpInventorySlot);

	PlayerInputComponent->BindAction("Throw", IE_Pressed, this, &AMinecraftCharacter::ThrowWieldedItem);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMinecraftCharacter::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("LMB", IE_Pressed, this, &AMinecraftCharacter::OnHit);
		PlayerInputComponent->BindAction("LMB", IE_Released, this, &AMinecraftCharacter::EndHit);
	}

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AMinecraftCharacter::OnResetVR);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMinecraftCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMinecraftCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMinecraftCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMinecraftCharacter::LookUpAtRate);
}

int32 AMinecraftCharacter::GetCurrentInventorySlot()
{
	return CurrentInventorySlot;
}

void AMinecraftCharacter::SetCurrentInventorySlot(int32 Slot) 
{
	CurrentInventorySlot = Slot;
}

float AMinecraftCharacter::Rounding(float Integer)
{
	int buff = Integer;
	float buff1 = 0.f;
	int integer = 0;
	float floatValue = 0.f;

	if(buff == 0) return 0;

	if (buff >= 0) {
		buff1 = (float)buff / 100;
		integer = buff1;
		floatValue = buff1 - integer;

		if (floatValue < 0.5) {
			integer = integer * 100;
		}
		else {
			integer = integer + 1;
			integer = integer * 100;
		}
	}
	else {
		buff = buff * -1;
		buff1 = (float)buff / 100;
		integer = buff1;
		floatValue = buff1 - integer;

		if (floatValue < 0.5) {
			integer = integer * 100;
		}
		else {
			integer = integer + 1;
			integer = integer * 100;
		}
		integer = integer * -1;
	}
	return integer;
}

void AMinecraftCharacter::DeleteItemFromInvetory()
{

	AWieldable* ItemToThrow = GetCurrentWieldedItem();

	if (ItemToThrow != NULL) {
		UWorld* const World = GetWorld();
		if (World != NULL) {
			Inventory[CurrentInventorySlot] = NULL;
		}
	}
	UpdateWieldedItem();
}

FVector AMinecraftCharacter::VectorCheck(FVector Cords)
{
	FVector CordsBuffer = Cords;
	FVector NewCords = FVector(0.f, 0.f, 0.f);

	return NewCords;
}

bool AMinecraftCharacter::AddItemToInventory(AWieldable * Item)
{
	if (Item){
		const int32 AvailableSlot = Inventory.Find(nullptr);

		if (AvailableSlot != INDEX_NONE) {
			for (int i = 0; i < NUM_OF_INVENTORY_SLOTS; i++) {
				if (Item->GetName().Find("Block") == 10 && Inventory[i] != NULL && (strncmp(TCHAR_TO_UTF8(*Inventory[i]->GetName()), TCHAR_TO_UTF8(*Item->GetName()), 20) == 0) && Inventory[i]->Count != 64) {
					Inventory[i]->Count++;
					SetCurrentInventorySlot(i);
					return true;
				}
				if (Item->Id == 49 && Inventory[i] != NULL && (strncmp(TCHAR_TO_UTF8(*Inventory[i]->GetName()), TCHAR_TO_UTF8(*Item->GetName()), 15) == 0) && Inventory[i]->Count != 16) {
					Inventory[i]->Count++;
					SetCurrentInventorySlot(i);
					return true;
				}
			}
			Inventory[AvailableSlot] = Item;
			SetCurrentInventorySlot(AvailableSlot);
			return true;
		}
	}
	return false;
}

UTexture2D * AMinecraftCharacter::GetThambnailAtInventorySlot(uint8 Slot)
{
	if (Inventory[Slot]){
		return Inventory[Slot]->PickupThumbnail;
	}
	return nullptr;
}

void AMinecraftCharacter::AddToCraftBuffer()
{

	AWieldable * AddedItem = GetCurrentWieldedItem();

	if (AddedItem != NULL && CurrentBlock->Id == 7 && CurrentBlock != NULL && CurrentBlock != nullptr) {
		CraftBuffer.Add(AddedItem);
		if(AddedItem->Count-- == 0){
			Inventory[CurrentInventorySlot] = NULL;
		}
		UpdateWieldedItem();
	}

}

void AMinecraftCharacter::AddToFurnace()
{
	AWieldable * AddedItem = GetCurrentWieldedItem();

	if (AddedItem != NULL && CurrentBlock->Id == 9 && CurrentBlock != NULL && CurrentBlock != nullptr) {
		FurnaceBuffer.Add(AddedItem);
		if (AddedItem->Count-- == 0) {
			Inventory[CurrentInventorySlot] = NULL;
		}
		UpdateWieldedItem();
	}

}

float AMinecraftCharacter::TimeToMelt(int Id)
{
	if(Id == 10) return 20.f;
	return 0.0f;
}

void AMinecraftCharacter::ClearCraftBuffer()
{

		CraftBuffer.Empty();

}

int AMinecraftCharacter::Craft()
{
	FString ItemName = "";
	int sum = 0;
	for (int i = 0; i < CraftBuffer.Num(); i++) {
		sum += CraftBuffer[i]->Id;
	}
	return sum;
}

void AMinecraftCharacter::OnFire()
{
	//// try and fire a projectile
	//if (ProjectileClass != NULL)
	//{
	//	UWorld* const World = GetWorld();
	//	if (World != NULL)
	//	{
	//		if (bUsingMotionControllers)
	//		{
	//			const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
	//			const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
	//			World->SpawnActor<AMinecraftProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
	//		}
	//		else
	//		{
	//			const FRotator SpawnRotation = GetControlRotation();
	//			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
	//			const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

	//			//Set Spawn Collision Handling Override
	//			FActorSpawnParameters ActorSpawnParams;
	//			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	//			// spawn the projectile at the muzzle
	//			World->SpawnActor<AMinecraftProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
	//		}
	//	}
	//}

	//// try and play the sound if specified
	//if (FireSound != NULL)
	//{
	//	UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	//}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AMinecraftCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMinecraftCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AMinecraftCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void AMinecraftCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void AMinecraftCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AMinecraftCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AMinecraftCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMinecraftCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AMinecraftCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMinecraftCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AMinecraftCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AMinecraftCharacter::TouchUpdate);
	}
	return bResult;
}

void AMinecraftCharacter::UpdateWieldedItem()
{

	Inventory[CurrentInventorySlot] != NULL ? FP_WieldedItem->SetSkeletalMesh(Inventory[CurrentInventorySlot]->WieldableMesh->SkeletalMesh) : FP_WieldedItem->SetSkeletalMesh(NULL);

}

AWieldable * AMinecraftCharacter::GetCurrentWieldedItem()
{
	return Inventory[CurrentInventorySlot] != NULL ? Inventory[CurrentInventorySlot] : nullptr;
}

void AMinecraftCharacter::SaveGame()
{
	
	UMySaveGame* SaveGame = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	SaveGame->PlayerLocation = this->GetActorLocation();
	UGameplayStatics::SaveGameToSlot(SaveGame, TEXT("SaveGame"), 0);
}

void AMinecraftCharacter::LoadGame()
{
	UMySaveGame* SaveGame = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));

	SaveGame = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot("SaveGame", 0));

	if(SaveGame != NULL){
		this->SetActorLocation(SaveGame->PlayerLocation);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, "GOOD");
	}else GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "BAD");
}

void AMinecraftCharacter::ThrowWieldedItem()
{

	AWieldable* ItemToThrow = GetCurrentWieldedItem();
	FHitResult LinetraceHit;

	FVector StartTrace = FirstPersonCameraComponent->GetComponentLocation();
	FVector EndTrace = (FirstPersonCameraComponent->GetForwardVector() * Reach) + StartTrace;

	FCollisionQueryParams CQP;
	CQP.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(LinetraceHit, StartTrace, EndTrace, ECollisionChannel::ECC_WorldDynamic, CQP);

	FVector DropLocation = EndTrace;

	if (LinetraceHit.GetActor() != NULL) {
		DropLocation = (LinetraceHit.ImpactPoint + 20.f);
	}

	if(ItemToThrow != NULL){
		UWorld* const World = GetWorld();
		if (World != NULL) {
			ItemToThrow->SetActorLocationAndRotation(DropLocation, FRotator::ZeroRotator);
			ItemToThrow->Hide(false);
			//if(ItemToThrow->Count-- == 0)
			Inventory[CurrentInventorySlot] = NULL;
		}
	}
	UpdateWieldedItem();
}

void AMinecraftCharacter::MoveUpInventorySlot()
{

	CurrentInventorySlot = FMath::Abs((CurrentInventorySlot + 1) % NUM_OF_INVENTORY_SLOTS);
	UpdateWieldedItem();
}

void AMinecraftCharacter::MoveDownInventorySlot()
{

	if (CurrentInventorySlot == 0) {
		CurrentInventorySlot = 9;
		UpdateWieldedItem();
		return;
	}
	CurrentInventorySlot = FMath::Abs((CurrentInventorySlot - 1) % NUM_OF_INVENTORY_SLOTS);
	UpdateWieldedItem();
}

void AMinecraftCharacter::OnHit()
{
	AWieldable* Item = GetCurrentWieldedItem();
	ABlock * BlockBuffer = CurrentBlock;
	float TimeBetweenBreaks = 0;

	PlayHitAnim();

	if (CurrentBlock != nullptr) {
		bIsBreaking = true;
		if (Item != NULL && Item->MaterialType >= CurrentBlock->MinimumMaterial) {
			if (Item->ToolType == CurrentBlock->RecommendedTool) {
				TimeBetweenBreaks = ((CurrentBlock->Resistance) / 100.f) / Item->MaterialType;
			}else TimeBetweenBreaks = ((CurrentBlock->Resistance * 3) / 100.f) / 1;
		}else if(CurrentBlock->MinimumMaterial == AWieldable::EMaterail::None){
			TimeBetweenBreaks = ((CurrentBlock->Resistance * 3) / 100.f) / 1;
		}else TimeBetweenBreaks = INT_MAX;

		GetWorld()->GetTimerManager().SetTimer(BlockBreakingHandle, this, &AMinecraftCharacter::BreakBlock, TimeBetweenBreaks, true);
		GetWorld()->GetTimerManager().SetTimer(HitAnimHandle, this, &AMinecraftCharacter::PlayHitAnim, 0.4f, true);
	}
}

void AMinecraftCharacter::EndHit()
{

	GetWorld()->GetTimerManager().ClearTimer(BlockBreakingHandle);
	GetWorld()->GetTimerManager().ClearTimer(HitAnimHandle);

	bIsBreaking = false;

	if (CurrentBlock != nullptr) {
		CurrentBlock->ResetBlock();
	}

}

void AMinecraftCharacter::PlayHitAnim()
{

	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}

}

void AMinecraftCharacter::CheckForBlocks()
{

	FHitResult LineTraceHit;
	
	FVector StartTrace = FirstPersonCameraComponent->GetComponentLocation();
	FVector EndTrace = (FirstPersonCameraComponent->GetForwardVector() * Reach) + StartTrace;


	FCollisionQueryParams CQP;
	CQP.AddIgnoredActor(this);

	GetWorld()->LineTraceSingleByChannel(LineTraceHit, StartTrace, EndTrace, ECollisionChannel::ECC_WorldDynamic, CQP);

	ABlock * PotentialBlock = Cast<ABlock>(LineTraceHit.GetActor());

	if (PotentialBlock != CurrentBlock && CurrentBlock != nullptr) {
		CurrentBlock->ResetBlock();
	}

	if (PotentialBlock == NULL) {
		CurrentBlock = nullptr;
		return;
	}
	else {
		if (CurrentBlock != nullptr && !bIsBreaking) {
			CurrentBlock->ResetBlock();
		}
		CurrentBlock = PotentialBlock;
	}
}

void AMinecraftCharacter::BreakBlock()
{

	if (bIsBreaking && CurrentBlock != nullptr && !CurrentBlock->IsPendingKill()) {
		CurrentBlock->Break();
	}

}
