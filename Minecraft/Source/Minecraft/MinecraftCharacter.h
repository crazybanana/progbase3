// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "Block.h"
#include "Wieldable.h"
#include "Recipe.h"
#include "MinecraftCharacter.generated.h"

class UInputComponent;

UCLASS(config=Game)
class AMinecraftCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;

public:
	AMinecraftCharacter();

	uint8 ToolType;
	uint8 MaterialType;

	void SetCurrentInventorySlot(int32 Slot);

protected:
	virtual void BeginPlay();

	virtual void Tick(float DeltaTime) override;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector GunOffset;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category=Projectile)
	TSubclassOf<class AMinecraftProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	class USoundBase* FireSound;

	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_WieldedItem;

	UFUNCTION(BlueprintPure, Category = HUD)
	int32 GetCurrentInventorySlot();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool AddItemToInventory(AWieldable* Item);

	UFUNCTION(BlueprintPure, Category = "Inventory")
	UTexture2D* GetThambnailAtInventorySlot(uint8 Slot);

	UPROPERTY(EditAnywhere)
	TArray<AWieldable*> Inventory;

	UPROPERTY(BlueprintReadWrite)
	ABlock * CurrentBlock;

	UPROPERTY(BlueprintReadWrite)
	bool bIsBreaking;

	UFUNCTION(BlueprintCallable)
	AWieldable* GetCurrentWieldedItem();

	UFUNCTION(BlueprintCallable)
	void SaveGame();

	UFUNCTION(BlueprintCallable)
	void LoadGame();

	UFUNCTION(BlueprintCallable)
	float Rounding(float Integer);

	UFUNCTION(BlueprintCallable)
	void DeleteItemFromInvetory();

	UFUNCTION(BlueprintCallable)
	FVector VectorCheck(FVector Cords);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray <AWieldable*> CraftBuffer;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TArray <AWieldable*> FurnaceBuffer;

	UFUNCTION(BlueprintCallable)
	void AddToCraftBuffer();

	UFUNCTION(BlueprintCallable)
	void AddToFurnace();

	UFUNCTION(BlueprintCallable)
	float TimeToMelt(int Id);

	UFUNCTION(BlueprintCallable)
	void ClearCraftBuffer();

	UFUNCTION(BlueprintCallable)
	int Craft();

	UPROPERTY(BlueprintReadWrite)
	bool Pickaxe;

protected:
	
	/** Fires a projectile. */
	void OnFire();

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

private:
	const int32 NUM_OF_INVENTORY_SLOTS = 10;

	int32 CurrentInventorySlot;

	void UpdateWieldedItem();

	void ThrowWieldedItem();

	void MoveUpInventorySlot();
	void MoveDownInventorySlot();

	void OnHit();
	void EndHit();

	void PlayHitAnim();

	void CheckForBlocks();

	void BreakBlock();

	float Reach;

	FTimerHandle BlockBreakingHandle;
	FTimerHandle HitAnimHandle;

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

};

