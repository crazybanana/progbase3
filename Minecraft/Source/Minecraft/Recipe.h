// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "Recipe.generated.h"

/**
 * 
 */
UCLASS()
class MINECRAFT_API URecipe : public UObject
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	URecipe();
	URecipe(int Recipes_Id, FString Recipes_Item);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Id;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Item;
	
	
};
