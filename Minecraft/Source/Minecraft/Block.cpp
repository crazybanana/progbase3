// Fill out your copyright notice in the Description page of Project Settings.

#include "Minecraft.h"
#include "Wieldable.h"
#include "MinecraftCharacter.h"
#include "Block.h"


// Sets default values
ABlock::ABlock()
{

	SM_Block = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh"));

	Resistance = 30.f;
	BreakingStage = 0.f;
	MinimumMaterial = AWieldable::EMaterail::None;
	Id = 0;

}

// Called when the game starts or when spawned
void ABlock::BeginPlay()
{
	Super::BeginPlay();
}

void ABlock::Break()
{
 	++BreakingStage;

	float CrackingValue = 1.0f - (BreakingStage / 5.f);

	UMaterialInstanceDynamic * MatInstance = SM_Block->CreateDynamicMaterialInstance(0);

	if (MatInstance != nullptr) {
		MatInstance->SetScalarParameterValue(FName("CrackingValue"), CrackingValue);
	}
	
	if (BreakingStage == 5.f) {
		AMinecraftCharacter * Character = Cast<AMinecraftCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
		ABlock* BlockToSpawn = Character->CurrentBlock;
		AWieldable* Item =  Character->GetCurrentWieldedItem();
		if (Character->CurrentBlock->RecommendedTool != AWieldable::ETool::Pickaxe) {
			OnBroken(true);
		}
		else if (Item->ToolType == AWieldable::ETool::Pickaxe && Character->CurrentBlock->RecommendedTool == AWieldable::ETool::Pickaxe) {
			OnBroken(true);
		}
		else {
			OnBroken(false);
		}
	}
}

void ABlock::ResetBlock()
{

	BreakingStage = 0.f;

	UMaterialInstanceDynamic * MatInstance = SM_Block->CreateDynamicMaterialInstance(0);

	if (MatInstance != nullptr) {
		MatInstance->SetScalarParameterValue(FName("CrackingValue"), 1.0f);
	}

}

void ABlock::OnBroken(bool HasRequiredPickaxe)
{
	AMinecraftCharacter * Character = Cast<AMinecraftCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
	if (HasRequiredPickaxe == true) {
		Destroy();
		Character->Pickaxe = HasRequiredPickaxe;
	}
	else {
		Destroy();
		Character->Pickaxe = HasRequiredPickaxe;
	}
}
