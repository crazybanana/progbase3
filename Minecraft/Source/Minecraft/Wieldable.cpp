// Fill out your copyright notice in the Description page of Project Settings.

#include "Minecraft.h"
#include "MinecraftCharacter.h"
#include "Wieldable.h"


// Sets default values
AWieldable::AWieldable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WieldableMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WieldableMesh"));
	PickupTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("PickupTrigger"));

	PickupTrigger->bGenerateOverlapEvents = true;
	PickupTrigger->OnComponentBeginOverlap.AddDynamic(this, &AWieldable::OnRadiusEnter);
	/*Uncomment to fix pickup triggers*/
	//PickupTrigger->AttachToComponent(WieldableMesh, FAttachmentTransformRules::SnapToTargetIncludingScale, TEXT("FP_WieldItem"));

	MaterialType = EMaterail::None;
	ToolType = ETool::Unarmed;

	bIsActive = true;

	Id = 0;

	Count = 0;
}

// Called when the game starts or when spawned
void AWieldable::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWieldable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FRotator rotation = WieldableMesh->GetComponentRotation();
	FVector location = WieldableMesh->GetComponentLocation();
	rotation.Yaw += 0.5;
	WieldableMesh->SetRelativeRotation(rotation);
}

void AWieldable::Hide(bool bVis)
{

	WieldableMesh->SetVisibility(!bVis);
	bIsActive = !bVis;

}

void AWieldable::OnUse()
{
	Destroy();
}

void AWieldable::OnRadiusEnter(UPrimitiveComponent * OverlapedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	
	if(bIsActive){
		AMinecraftCharacter * Character = Cast<AMinecraftCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
		if(Character->GetActorLocation() == OtherActor->GetActorLocation()){
			Character->FP_WieldedItem->SetSkeletalMesh(WieldableMesh->SkeletalMesh);
			Character->AddItemToInventory(this);
			Hide(true);
		}
	}
}




