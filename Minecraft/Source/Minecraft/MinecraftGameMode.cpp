// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Minecraft.h"
#include "MinecraftGameMode.h"
#include "MinecraftHUD.h"
#include "Blueprint/UserWidget.h"
#include "MinecraftCharacter.h"

void AMinecraftGameMode::BeginPlay()
{

	Super::BeginPlay();

	ApplyHUDChanges();

}

AMinecraftGameMode::AMinecraftGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMinecraftHUD::StaticClass();

	HUDState = EHUDState::HS_Ingame;
}

void AMinecraftGameMode::ApplyHUDChanges()
{

	if (CurrentWidget != nullptr) {
		CurrentWidget->RemoveFromParent();
	}

	switch (HUDState)
	{
		case EHUDState::HS_Ingame: {
			ApplyHUD(IngameHUDClass, false, false);
		}
		case EHUDState::HS_Inventory: {
			ApplyHUD(InventoryHUDClass, true, true);
		}
		case EHUDState::HS_Craft_Menu: {
			ApplyHUD(CraftMenuHUDClass, true, true);
		}
		default:
			ApplyHUD(IngameHUDClass, false, false);
	}

}

uint8 AMinecraftGameMode::GetHUDState()
{
	return HUDState;
}

void AMinecraftGameMode::ChangeHUDState(uint8 NewState)
{

	HUDState = NewState;
	ApplyHUDChanges();

}

bool AMinecraftGameMode::ApplyHUD(TSubclassOf<class UUserWidget> WidgetToApply, bool ShowMouseCursor, bool EnableClickEvents)
{
	AMinecraftCharacter* MyCharacter = Cast<AMinecraftCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));
	APlayerController* MyController = GetWorld()->GetFirstPlayerController();

	if (WidgetToApply != nullptr) {
		MyController->bShowMouseCursor = ShowMouseCursor;
		MyController->bEnableClickEvents = EnableClickEvents;

		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetToApply);

		if (CurrentWidget != nullptr) {
			CurrentWidget->AddToViewport();
			return true;
		}else return false;
	}else return false;
}
