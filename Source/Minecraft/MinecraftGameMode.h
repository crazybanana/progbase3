// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "MinecraftGameMode.generated.h"

UCLASS(minimalapi)
class AMinecraftGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMinecraftGameMode();
};



